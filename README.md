# Trabajo Profesional

## Sistema de control para un horno de inducción

### Integrantes

* Martín Cruz García
* Federico Manuel Gomez Peter
* Mariana Szischik


### Firmware 

Este repositorio contiene el Firmware desarrollado para la solución propuesta. Fue desarrollado para un microcontrolador Texas Instruments TM4C123GH6PMI, aunque podría ser compatible con otros dispositivos de la familia con algunas pequeñas modificaciones.

### Dependencias

Se recomienda utilizar Code Composer Studio para desarrollar sobre este proyecto, así como para compilar y debuggear.

Para compilar es necesaria la biblioteca TivaWare, que se puede descargar del sitio web de Texas Instruments. En particular, se necesita linkear correctamente las *driverlib* y *usblib*. Esto se puede configurar fácilmente desde las propiedades de proyecto de Code Composer Studio. Además es necesario configurar el dispositivo específico para el cual se desea compilar y corroborar que están definidos algunos flags de compilación, según se ve en las imágenes.

![Configuración del dispositivo](images/dispositivo.PNG "Configuración del dispositivo")

![Configuración de flags de compilador](images/define.PNG "Configuración de flags de compilador")

![Configuración de include path](images/include.PNG "Configuración de include path")

![Configuración del linker](images/linker.PNG "Configuración del linker")